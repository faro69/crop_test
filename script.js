const fileInput = document.getElementById("fileInput");
const cropButton = document.getElementById("cropButton");
let img;
let canvas;
let leftTop = {x: 0, y: 0};
let rightBottom = {x: 0, y: 0};
let clickPoint;
let prevMovePoint;
const POINT_RADIUS = 10;
let movingType;

const getRectSize = () => ({
    width: rightBottom.x - leftTop.x,
    height: rightBottom.y - leftTop.y,
})

cropButton.addEventListener('click', () => {
    const {width, height} = getRectSize();
    let subCanvas = document.createElement('canvas');
    subCanvas.width = width;
    subCanvas.height = height;
    let subContext = subCanvas.getContext('2d');
    subContext.drawImage(img, leftTop.x, leftTop.y, width, height, 0, 0, width, height);
    subCanvas.toBlob((blob) => {
        var fd = new FormData();
        blob.lastModifiedDate = new Date();
        blob.name = 'image.png';
        fd.append('attachment', blob);
        fetch('https://x5test.vvdev.ru/api/attachments/upload/photo', {
            method: 'POST',
            body: fd,
            headers: new Headers({
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiJSZVZ5alpLYlhxIiwidXNlcm5hbWUiOiJ2dmRldiIsImlhdCI6MTU2ODMxMTA0Mn0.b-NJ1FyrB5MV0Qxa_o0CSxf1qeSbvhWMVI4W6uIrR-s', 
            }), 
        }).then(() => {console.log('success')})
    }, 'image/png');
});


const drawPoint = (point, radius) => {
    const ctx = canvas.getContext('2d');
    ctx.beginPath();
    ctx.arc(point.x, point.y, radius, 0, Math.PI * 2)
    ctx.fill();

    ctx.beginPath();
    ctx.rect(leftTop.x, leftTop.y, rightBottom.x - leftTop.x, rightBottom.y - leftTop.y);
    ctx.stroke();
}

const draw = () => {
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);
    drawPoint(leftTop, POINT_RADIUS);
    drawPoint(rightBottom, POINT_RADIUS);
}

const isPointNear = (p1, p2, radius) => {
    const deltaX = Math.abs(p1.x - p2.x);
    const deltaY = Math.abs(p1.y - p2.y);
    return deltaX < radius && deltaY < radius;
}

const onCanvasMouseDown = (e) => {
    const {layerX: x, layerY: y} = e;
    clickPoint = {x,y};
    prevMovePoint = {x,y};

    if (x >= leftTop.x && x <= rightBottom.x && y >= leftTop.y && y <= rightBottom.y) {
        movingType = 'center';  
    }

    if (isPointNear(leftTop, clickPoint, POINT_RADIUS)) {
        movingType = 'leftTop';
    }

    if (isPointNear(rightBottom, clickPoint, POINT_RADIUS)) {
        movingType = 'rightBottom';
    }
}

const onCanvasMouseUpAndOut = (e) => {
    movingType = null;
    clickPoint = null;
    prevMovePoint = null;
}

const onCanvasMouseMove = (e) => {
    if (movingType) {
        const {layerX: x, layerY: y} = e;
        const movePoint = {x,y};

        const deltaX = x - prevMovePoint.x;
        const deltaY = y - prevMovePoint.y;
        const maxDelta = Math.max(Math.abs(deltaX), Math.abs(deltaY));
        let increaser = 0;
        if (deltaX > 0 || deltaY > 0) {
            increaser = maxDelta;
        } else if (deltaX < 0 || deltaY < 0) {
            increaser = -maxDelta;
        }

        switch (movingType) {
            case 'center': {
                const newLeftTop = {
                    x: leftTop.x + deltaX,
                    y: leftTop.y + deltaY,
                }

                const newRightBottom = {
                    x: rightBottom.x + deltaX,
                    y: rightBottom.y + deltaY
                };

                if (newLeftTop.x >= 0 && 
                    newLeftTop.y >= 0 && 
                    newRightBottom.x <= img.width && 
                    newRightBottom.y <= img.height
                ) {
                
                    leftTop = newLeftTop;
                    rightBottom = newRightBottom;
                }
                break;
            }
            case 'leftTop': {
                const newLeftTop = {
                    x: leftTop.x + increaser,
                    y: leftTop.y + increaser,
                }
                if (
                    newLeftTop.x > 0 && 
                    newLeftTop.y > 0 &&
                    newLeftTop.x < rightBottom.x && 
                    newLeftTop.y < rightBottom.y    
                ) {
                    leftTop = newLeftTop;
                }
                break;
            }   
            case 'rightBottom': {
                const newRightBottom = {
                    x: rightBottom.x + increaser,
                    y: rightBottom.y + increaser,
                }
                if (
                    newRightBottom.x < img.width && 
                    newRightBottom.y < img.height &&
                    newRightBottom.x > leftTop.x && 
                    newRightBottom.y > leftTop.y
                ) {
                    rightBottom = newRightBottom;
                }
                break;
            }
            default:
        }
        draw();
        prevMovePoint = movePoint;
    }
}

const initCanvas = (file) => {
    canvas = document.getElementsByTagName('canvas')[0];
    if (!canvas) {
        canvas = document.createElement('canvas');
        canvas.style.display = 'block';
        document.body.appendChild(canvas);
        canvas.addEventListener('mousedown', onCanvasMouseDown);
        canvas.addEventListener('mousemove', onCanvasMouseMove);
        canvas.addEventListener('mouseup', onCanvasMouseUpAndOut);
        canvas.addEventListener('mouseout', onCanvasMouseUpAndOut);
    }
    img = new Image();
    img.src = URL.createObjectURL(file);
    img.onload = function() {
        cropButton.removeAttribute('disabled');
        const {width, height} = img;
        canvas.width = width;
        canvas.height = height;
        rightBottom = {x: width / 2, y: height / 2};
        draw();
    }
}

fileInput.addEventListener('change', () => {
    const [imgFile] = fileInput.files;
    if (imgFile) {
        const ext = imgFile.name.split(".").reverse()[0];
        if (['jpg', 'png'].includes(ext)) {
            initCanvas(imgFile);
        } else {
            alert('Загрузите jpg или png')
        }
    }
})

